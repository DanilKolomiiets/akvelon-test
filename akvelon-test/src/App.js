import './App.css';
import InvoiceList from './Components/InvoiceList'
import {useState, useEffect} from 'react'
import CreateInvoice from './Components/CreateInvoice';

function App() {
  const [data, setData] = useState(null)
  const [action, setAction] = useState(false)


  useEffect(()=>{
    fetch('http://localhost:8000/data')
      .then(res => {
        return res.json()
      })
      .then((Data)=>{
        setData(Data)
      })
  },[])



  const closeAddInovice = (e) => {
    setAction(e)
    document.location.reload();
  }




  const openAction = () => {
    setAction(true)
  }
  return (
    <div className="App">  
      {action ? 
        <div>
           <div className='title-area'>
              <h2><span>Create invoice</span></h2>
          </div>
          <CreateInvoice action={closeAddInovice}/>
        </div>
        :
        <div>
          <div className='title-area'>
            <h2><span>Invoices</span></h2>
          </div>
          <div className='action-area'>
            <h3>Actions</h3>
            <button onClick={openAction}>Add new</button>
          </div>
          {data && <InvoiceList data={data}/>}
        </div>
        }
     
    </div>
  );
}

export default App;
