import './CreateInvoice.css';
import {useState} from 'react'


const CreateInvoice = ({action}) => {

    const [state, setState] = useState(0)

    function createInvoice(opts) {  
        fetch('http://localhost:8000/data', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(opts)
        })
        .then(function(response) {
          return response.json();
        })
      }
      
      function submitInvoice(e) {
          e.preventDefault()
        if(state >= 3){
            action(false)
            createInvoice(showValue());
        }else{
            alert('not enough')
        }
      }

      const lengthFilter = ()=> {
          let length = document.querySelector('#invNumber').value.length
          setState(length)
        }

      const  makeId = () => {
        let ID = "";
        let characters = "abcdefghijklmnopqrstuvwxyz0123456789";
        for ( var i = 0; i < 20; i++ ) {
          ID += characters.charAt(Math.floor(Math.random() * 36));
        }
        return ID;
      }
    const showValue = () =>{
        let id = makeId()
        let data = {
            "id": id,
            "number": '',
            "date_created": '',
            "date_supplied": '',
            "comment": ''
        }

        const inputs = document.querySelectorAll('input')
        inputs.forEach(i => {
           switch(i.id) {
               case 'invNumber':
                   data.number = parseInt(i.value)
                   break
                case 'invDate':
                    data.date_created = i.value.split('-').reverse().join('-')
                    break
                case 'supplyDate':
                    data.date_supplied = i.value.split('-').reverse().join('-')
                    break
                case 'comment':
                    data.comment = i.value
                    break
                default:
                    break
           }
        })  
        return (data)

    }



  return (
      <div className="create-area">
          <div className="create-window">
              <form onSubmit={submitInvoice}>
                <div className='dates-container'>
                    <div className="input-box">
                        <span className="prefix">INV-</span>
                        <input placeholder="Number" type='number' id='invNumber' onChange={lengthFilter}/>
                    </div>
                    <input placeholder="Inv date" type='date' id='invDate'/>
                    <input placeholder="Supply date" type='date' id='supplyDate'/>
                </div>  
                <input placeholder="Comments" type='text' id='comment' maxLength="160"/>
                <button id ='submit' type='submit'>Save</button>
              </form>
            </div>
      </div>
  )
}

export default CreateInvoice;
