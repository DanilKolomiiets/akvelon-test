
import './InvoiceList.css';

function InvoiceList({data}) {
  return (
  <div className="table-area">
    <h3>Invoices</h3>
    <table>
        <thead className="table-head">
            <tr>
                <th>Create</th>
                <th>No</th>
                <th>Supply</th>
                <th>Comment</th>
            </tr>
        </thead>
        <tbody>
            {data.map(invoice => (
                <tr key ={invoice.number} className='table-item'>
                    <th>{invoice.date_created}</th>
                    <th>INV {invoice.number}</th>
                    <th>{invoice.date_supplied}</th>
                    <th>{invoice.comment}</th>
                </tr>
            ))}
        </tbody>

    </table>
  </div>
  );
}

export default InvoiceList;
